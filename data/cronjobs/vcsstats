#!/bin/sh
# CRON=0 0 * * *

set -eu

psql -Xq service=qa <<EOF
INSERT INTO vcsstats
SELECT now() AS ts, vcs, status, count(*) FROM vcs
WHERE vcs IS NOT NULL
GROUP BY vcs, status
ORDER BY vcs, status;
EOF

DATA=$(mktemp --tmpdir vcsstats.XXXXXX)
trap "rm -f $DATA" EXIT

VCSLIST=$(psql service=qa -U guest -XAtc 'SELECT DISTINCT vcs FROM vcsstats ORDER BY 1')

# Overview graph

( echo 'SELECT ts::date, vcs, sum(count), dense_rank() OVER (ORDER BY vcs) FROM vcsstats GROUP BY 1, 2 ORDER BY 1, 2'
  echo '\\crosstabview 1 2 3 4'
) | psql service=qa -U guest -XAtF "	" > $DATA

LINES=$(wc -l $DATA)
EVERY=$((${LINES% *} / 15))
[ "$EVERY" = 0 ] && EVERY=1

(
  cat <<-EOF
	set terminal png size 600,500
	set output "/srv/qa.debian.org/web/img/vcswatch.png"
	set datafile separator '\t'
	set xdata time
	set timefmt "%Y-%m-%d"
	set format x "%Y-%m"
	set pointsize 1.5
	set xrange [:]
	set key below
	set title "Vcswatch packages"
	plot \\
EOF

  n=2 # start with 2nd csv column
  need_comma=""
  for vcs in $VCSLIST; do
    [ "$need_comma" ] && echo ", \\"
    echo "\"$DATA\" using 1:$n notitle with lines linetype $n, \\"
    echo "\"$DATA\" every $EVERY using 1:$n title \"$vcs\" with points linetype $n \\"
    n=$((n + 1))
    need_comma=1
  done
) | gnuplot -

# individual graphs

for vcs in $VCSLIST; do

  STATUSLIST=$(psql service=qa -U guest -XAtc "SELECT DISTINCT status FROM vcsstats WHERE vcs = '$vcs' ORDER BY 1")

  ( echo "SELECT ts::date, status, sum(count), dense_rank() OVER (ORDER BY status) FROM vcsstats WHERE vcs = '$vcs' GROUP BY 1, 2 ORDER BY 1, 2"
    echo '\\crosstabview 1 2 3 4'
  ) | psql service=qa -U guest -XAtF "	" > $DATA

  LINES=$(wc -l $DATA)
  EVERY=$((${LINES% *} / 15))
  [ "$EVERY" = 0 ] && EVERY=1

  (
    cat <<-EOF
	set terminal png size 600,500
	set output "/srv/qa.debian.org/web/img/vcswatch-$vcs.png"
	set datafile separator '\t'
	set xdata time
	set timefmt "%Y-%m-%d"
	set format x "%Y-%m"
	set pointsize 1.5
	set xrange [:]
	set key below
	set title "$vcs packages"
	plot \\
EOF

    n=2 # start with 2nd csv column
    need_comma=""
    for status in $STATUSLIST; do
      [ "$need_comma" ] && echo ", \\"
      echo "\"$DATA\" using 1:$n notitle with lines linetype $n, \\"
      echo "\"$DATA\" every $EVERY using 1:$n title \"$status\" with points linetype $n \\"
      n=$((n + 1))
      need_comma=1
    done
  ) | gnuplot -

done
